import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-two',
  templateUrl: './form-two.component.html',
  styleUrls: ['./form-two.component.scss']
})
export class FormTwoComponent implements OnInit {
  @Output()
  public submitFormTwoEvent = new EventEmitter<any>();

  public formTwo: FormGroup;

  constructor() {
    this.formTwo = new FormGroup({
      type: new FormControl('', [Validators.minLength(2),Validators.required]),
      category: new FormControl('', [Validators.minLength(2),Validators.required]),
    });
  }

  ngOnInit() {}

  public onSubmit(){
    console.log(this.formTwo.value)
    console.log(this.formTwo.valid)
    if(this.formTwo.valid){
      this.submitFormTwoEvent.emit(this.formTwo.value);
    } else {
      console.log("algun campo es invalido")
    }
  }
}
