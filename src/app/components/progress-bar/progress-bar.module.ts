// Modules
import { NgModule } from '@angular/core';

// Components
import { ProgressBarComponent } from './progress-bar.component';


@NgModule({
  declarations: [
    ProgressBarComponent,
  ],

  imports: [

  ],

  exports: [
    ProgressBarComponent
  ],

  providers: [ ],

})

export class ProgressBarModule { }