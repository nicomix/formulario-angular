import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  /*
  Evento para emitir los datos del formulario al componente padre,
  que en este caso será la página home. No hago una petición post
  al backend directamente desde este componente porque podría ser usado
  en diferentes páginas y apuntar a diferentes url de backend.
  */
  @Output()
  public submitEvent = new EventEmitter<any>();

  /*
  FormGroup para administrar los campos del formulario, poniendo validadores
  que pueden ser usados para enriquecer la funcionalidad del componente.
  Por ejemplo si el campo email no es un email podríamos analizar con un condicional
  si el formulario es valido o no para no permitir hacer submit y mostrar un mensaje
  de que el email debe ser un email.
  Este FormGroup debes indicarlo en tu archivo .html en la etiqueta <form> con [formGroup]="registerForm"
  */
  public registerForm: FormGroup;

  constructor() {
    this.registerForm = new FormGroup({
      // FormControl asociado a cada campo del formulario, debes indicarlo en tu etiqueta <input> con formControlName="firstName"
      firstName: new FormControl('', [Validators.minLength(2),Validators.required]),
      // Por ejemplo este form control tiene un validador para por lo menos 2 caracteres
      lastName: new FormControl('', [Validators.minLength(2),Validators.required]),
      // Este form control exije que el campo email contenga un string tipo email
      email: new FormControl('', [Validators.email,Validators.required]),
      // Y este que el campo contenga un string solo numerico
      phone: new FormControl('', [Validators.pattern('^[0-9]*$'),Validators.required]),
    });
  }

  ngOnInit() {}

  public onSubmit(){
    // Console log para comprobar en consola que se va a emitir, esto obvio hay que borrarlo
    console.log(this.registerForm.value)
    /*
    Si un campo del formulario no es valid, el atributo "valid" del FormGroup registerForm
    será un boolean falso, si todos los campos son validos entonces "valid" será true.
    Con el atributo valid entonces se puede enriquecer el código con mas funcionalidades
    al usarlo para analizar condiciones.
    */
    console.log(this.registerForm.valid)
    // Condicional para solo emitir el evento cuando el formulario es valido
    if(this.registerForm.valid){
      // Emitir el evento para que el componente padre lo capture
      this.submitEvent.emit(this.registerForm.value);
    } else {
      console.log("algun campo es invalido")
    }
  }
}
