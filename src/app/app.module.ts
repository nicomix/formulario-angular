import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormTwoComponent } from './components/form-two/form-two.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    /* No olvides importar ReactiveFormsModule y dependiendo de si usas
    lazy loading o no debes importarlo en la página donde lo uses
    por ejemplo la página home */
    ReactiveFormsModule,
    /*
    Tambien debes siempre importar HttpClientModule para poder usar
    el servicio HttpClient y comunicarte con tu backend por medio de REST.
    */
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
