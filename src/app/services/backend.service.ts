import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(
    // Importamos httpClient para poder hacer peticiones post, get, put, delete al backend
    private httpClient: HttpClient,
  ) { }

  public saveForm(formInfo: any){
    console.log("formulario enviado")
    return this.httpClient.post<any>("http://backendsensual.com.co", formInfo);
  }
}
