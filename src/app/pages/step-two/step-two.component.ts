import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit {
  constructor(
    // Importamos el servicio donde tenemos las funciones asociadas al backend
    private backendService: BackendService,
  ) { }

  ngOnInit(): void {
  }

  /*
  Acá capturamos el evento y gestionamos la información por ejemplo para
  usar el servicio BackendService y hacer una petición post hacia el backend
  */
  public onFormSubmit(formInfo: any) {
    this.backendService.saveForm(formInfo)
  }

}
