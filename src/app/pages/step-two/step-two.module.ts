import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StepTwoRoutingModule } from './step-two-routing.module';
import { StepTwoComponent } from './step-two.component';
import { FormTwoComponent } from 'src/app/components/form-two/form-two.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgressBarComponent } from 'src/app/components/progress-bar/progress-bar.component';
import { ProgressBarModule } from 'src/app/components/progress-bar/progress-bar.module';


@NgModule({
  declarations: [
    StepTwoComponent,
    FormTwoComponent,
  ],
  imports: [
    CommonModule,
    StepTwoRoutingModule,
    ReactiveFormsModule,
    ProgressBarModule,
  ]
})
export class StepTwoModule { }
