import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RegisterFormComponent } from 'src/app/components/register-form/register-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgressBarComponent } from 'src/app/components/progress-bar/progress-bar.component';
import { ProgressBarModule } from 'src/app/components/progress-bar/progress-bar.module';


@NgModule({
  declarations: [
    HomeComponent,
    RegisterFormComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    ProgressBarModule,
  ]
})
export class HomeModule { }
