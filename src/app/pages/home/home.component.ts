import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    // Importamos el servicio donde tenemos las funciones asociadas al backend
    private backendService: BackendService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
  }

  /*
  Acá capturamos el evento y gestionamos la información por ejemplo para
  usar el servicio BackendService y hacer una petición post hacia el backend
  */
  public onFormSubmit(formInfo: any) {
    this.backendService.saveForm(formInfo)
    this._router.navigateByUrl('/step-two')
  }

}
